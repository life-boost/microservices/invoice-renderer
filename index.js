const express = require('express');
const puppeteer = require('puppeteer');
const Twig = require('twig');
const app = express();

const PORT = 8080;
const HOST = '0.0.0.0';

app.use(express.json());
app.set("twig options", {
    allowAsync: true,
    strict_variables: false
});

app.post('/html-to-pdf', async (req, res) => {
    Twig.renderFile('./templates/invoice.html.twig', req.body, (err, html) => {
        if (err) {
            res.send(err);

            return;
        }

        convertHtmlToPdf(html).then(pdf => {
            res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length });
            res.send(pdf);
        });
    });
});

const convertHtmlToPdf = async (html) => {
    const browser = await puppeteer.launch({ headless: 'new' });
    const page = await browser.newPage();
    await page.setContent(html, {waitUntil: 'networkidle0'});
    const pdf = await page.pdf({ format: 'A4' });

    await browser.close();

    return pdf;
}

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});
